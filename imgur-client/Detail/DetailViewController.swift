//
//  DetailViewController.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/9/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import ReSwift
import Imaginary

class DetailViewController: UIViewController {
    
    var imageChoices: [ImageModel]?
    var spinner = UIActivityIndicatorView(style: .gray)
    
    @IBOutlet weak var scrollView: UIScrollView!
    var stackView = UIStackView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureScrollAndStackViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        store.subscribe(self) {
            $0.select { $0.detailState }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        store.unsubscribe(self)
        if isMovingFromParent {
            store.dispatch(DetailAction.SetSelected(image: nil))
        }
    }
    
    fileprivate func configureScrollAndStackViews() {
        guard let scrollView = scrollView else { return }
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.delegate = self
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
        
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.distribution = .equalSpacing
        scrollView.addSubview(stackView)
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)])
    }
    
    
    
    fileprivate func viewsForStackView(from images: [ImageModel]?) -> [UIView] {
        var views = [UIView]()
        guard let images = images else { return views }
        
        for image in images {
            if let label = titleLabel(for: image) {
                views.append(label)
            }
            
            switch image.type {
            case .imageGif?:
                if let imageView = imageViewFor(gif: image) {
                    views.append(imageView)
                }
            case .imagePng?, .imageJpeg?, .imageJpg?:
                if let imageView = imageViewFor(jpgOrPng: image) {
                    views.append(imageView)
                }
            default:
                let imageView = ImageView()
                imageView.image = UIImage(placeholder: .notFound)
                views.append(imageView)
            }
        }
        return views
    }
    
    fileprivate func imageViewFor(gif image: ImageModel) -> ImageView? {
        guard image.type == ImageAssetType.imageGif else { return nil }
        
        let imageView = ImageView()
        
        guard let imageUrl = image.link else {
            imageView.image = UIImage(placeholder: .notFound)
            return imageView
        }
        
        imageView.addSubview(spinner)
        spinner.center = view.center
        spinner.startAnimating()
        
        DispatchQueue.global(qos: .userInitiated).async {
            // TODO: make GIFs cache
            let image = UIImage.gifImageWithURL(imageUrl)
            DispatchQueue.main.async { [weak self] in
                imageView.image = image
                self?.imageLoaded()
            }
        }
        return imageView
    }
    
    fileprivate func imageViewFor(jpgOrPng image: ImageModel) -> ImageView? {
        guard image.type == ImageAssetType.imageJpg ||
            image.type == ImageAssetType.imageJpeg ||
            image.type == ImageAssetType.imagePng else { return nil }
        
        let imageView = ImageView()
        
        guard let imageUrl = URL(string: image.link ?? "") else {
            imageView.image = UIImage(placeholder: .notFound)
            return imageView
        }
        
        imageView.addSubview(spinner)
        spinner.center = view.center
        spinner.startAnimating()
        
        imageView.setImage(url: imageUrl) { [weak self] result in
            switch result {
            case .error:
                imageView.image = UIImage(placeholder: .notFound)
            default: break
            }
            self?.imageLoaded()
        }
        return imageView
    }
    
    fileprivate func titleLabel(for image: ImageModel) -> UILabel? {
        guard let imageTitle = image.title, imageTitle != self.title else { return nil }
        // dont show the image title, if it matches the navigation bar title
        let label = UILabel()
        label.numberOfLines = 0
        label.text = title
        return label
    }
    
    fileprivate func imageLoaded() {
        spinner.stopAnimating()
        spinner.removeFromSuperview()
    }
}

extension DetailViewController: StoreSubscriber {
    func newState(state: DetailState) {
        guard let image = state.image else {
            // nothing selected to show, pop back
            navigationController?.popViewController(animated: true)
            return
        }
        title = image.title ?? "(Untitled)"
        stackView.removeAllArrangedViews()
        let viewsToAdd = viewsForStackView(from: image.allImages)
        
        for view in viewsToAdd {
            view.translatesAutoresizingMaskIntoConstraints = false
            view.updateConstraints()
            stackView.addArrangedSubview(view)
        }
    }
}

extension DetailViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return stackView
    }
}
