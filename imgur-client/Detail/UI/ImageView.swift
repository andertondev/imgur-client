//
//  ImageView.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/9/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import UIKit
class ImageView: UIImageView {
    fileprivate var aspectRatioConstraint:NSLayoutConstraint?
    override func updateConstraints() {
        super.updateConstraints()
        
        var aspectRatio: CGFloat = 1
        if let image = image {
            aspectRatio = image.size.width / image.size.height
        }
        
        aspectRatioConstraint?.isActive = false
        aspectRatioConstraint =
            widthAnchor.constraint(
                equalTo: heightAnchor,
                multiplier: aspectRatio)
        aspectRatioConstraint?.isActive = true
    }
}

