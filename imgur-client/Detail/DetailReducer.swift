//
//  SearchReducer.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/5/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import ReSwift

func detailReducer(action: Action, state: DetailState?) -> DetailState {
    var state = state ?? DetailState()
    switch action {
    case let action as DetailAction.SetSelected:
        state.image = action.image
    default: break
    }
    return state
}
