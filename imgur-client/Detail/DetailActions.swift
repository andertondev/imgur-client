//
//  SearchActions.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/8/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import ReSwift

struct DetailAction {
    struct SetSelected: Action {
        let image: ImageSearchResultModel?
    }
}
