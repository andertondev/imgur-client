//
//  SearchState.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/5/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import ReSwift

struct DetailState: StateType {
    var image: ImageSearchResultModel?
}
