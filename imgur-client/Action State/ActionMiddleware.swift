//
//  ActionMiddleware.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/5/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import ReSwift
let actionMiddleware: Middleware<StateType> = { dispatch, getState in
    return { next in
        return { action in
            #if DEBUG
//            print(action.description)
            #endif
            next(action)
        }
    }
}

fileprivate extension Action {
    var description: String {
        return String(reflecting: type(of: self))
    }
}

