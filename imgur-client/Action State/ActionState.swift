//
//  ActionState.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/5/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import ReSwift

struct ActionState: Action {
    var lastAction: Action?
}

