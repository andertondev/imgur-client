//
//  ActionReducer.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/5/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import ReSwift
func actionReducer(action: Action, state: ActionState?) -> ActionState {
    var state = state ?? ActionState()
    state.lastAction = action
    return state
}
