//
//  SearchReducer.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/5/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import ReSwift

func searchReducer(action: Action, state: SearchState?) -> SearchState {
    var state = state ?? SearchState()
    switch action {
    case let action as SearchAction.SetQuery:
        state.query = action.query
        state.debounceDelay?.cancel()
        state.page = 0
        state.lastPageFound = false
        state.images?.removeAll()
        guard let query = action.query, !query.isEmpty else {
            state.status = .notStarted
            break
        }
        state.debounceDelay = DispatchWorkItem {
            store.dispatch(SearchAction.Fetch())
        }
        state.status = .debouncing
    case let action as SearchAction.Fetch:
        if let query = action.query {
            state.query = query
        }
        state.status = .inProgress
    case _ as SearchAction.FetchNextPage:
        state.page += 1
    case let action as SearchAction.HandleResponse:
        state.status = .completed(action.response)
        if action.response.success {
            let previousImages = state.images ?? []
            let fetchedImages = action.response.data?.compactMap { return $0 } ?? []
            state.images = previousImages + fetchedImages
            state.lastPageFound = fetchedImages.count == 0
        }
        break
    case _ as SearchAction.FetchError:
        state.status = .failed(.unknown)
    case _ as SearchAction.Rendered:
        state.status = .rendered
    case _ as SearchAction.NotStarted:
        state.status = .notStarted
    default: break
    }
    return state
}
