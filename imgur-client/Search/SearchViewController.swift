//
//  SearchViewController.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/5/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import ReSwift
import AVFoundation
import MediaPlayer
import AVKit

typealias SearchSubscribedState = (searchState: SearchState, detailState: DetailState)

class SearchViewController: UIViewController {
    
    var images: [ImageSearchResultModel]?
    var isSearching = false
    @IBOutlet var searchSpinner: UIActivityIndicatorView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        store.subscribe(self) {
            $0.select { (
                searchState: $0.searchState,
                detailState: $0.detailState)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        store.unsubscribe(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Search"
    }
}


extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        store.dispatch(SearchAction.SetQuery(query: searchBar.text))
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        store.dispatch(SearchAction.SetQuery(query: nil))
        view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let query = searchBar.text else { return }
        store.dispatch(SearchAction.Fetch(query: query))
        view.endEditing(true)
    }
}

extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let image = images?[indexPath.item] else {
            let alert = UIAlertController(title: "Error", message: "Sorry, we couldn't find the image you tried to load", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { _ in }))
            return
        }
        
        
        switch image.firstType {
        case .videoMp4?:
            guard let url = URL(string: image.allImages.first?.link ?? "") else {
                let alert = UIAlertController(title: "Error", message: "Sorry, there was an error loading the video", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { _ in }))
                break
            }
            presentVideoPlayer(for: url)
        default:
            store.dispatch(DetailAction.SetSelected(image: image))
        }
    }
    
    fileprivate func presentVideoPlayer(for url: URL) {
        let player = AVPlayer(url: url)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player?.play()
        }    
    }
}

extension SearchViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return images?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell", for: indexPath) as! SearchTableViewCell
        cell.thumbnailImageView?.image = nil
        cell.model = images?[indexPath.item]
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let images = images else { return }
        guard images.count > 1 else { return }
        guard !store.state.searchState.lastPageFound else { return }
        
        switch store.state.searchState.status {
        case .rendered:
            let last = images.count - 1
            if indexPath.item == last {
                store.dispatch(SearchAction.FetchNextPage())
            }
        default: break
        }
    }
    
    fileprivate func tableFooterView(for status: SearchStatus) -> UIView? {
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        let loadedImageCount = images?.count ?? 0
        
        switch status {
        case .notStarted:
            label.text = "Enter a search term"
        case .inProgress:            
            label.text = loadedImageCount == 0 ? "Searching" : "Loading more images"
        case .rendered:
            if loadedImageCount == 0 {
                label.text = "No results found"
            }
        default: break
        }
        
        return (label.text != nil) ? label : UIView()
    }
}

extension SearchViewController: StoreSubscriber {
    
    
    func newState(state: SearchSubscribedState) {
        
        guard state.detailState.image == nil else {
            let storyboard = UIStoryboard(storyboard: .detail)
            let viewController = storyboard.instantiateViewController() as DetailViewController
            navigationController?.pushViewController(viewController, animated: true)
            view.endEditing(true)
            return
        }
        
        tableView.tableFooterView = tableFooterView(for: state.searchState.status)
        
        switch state.searchState.status {
        case .debouncing, .inProgress, .rendered:
            break
        case .notStarted:
            images?.removeAll()
            tableView.reloadData()
        case .completed:
            images = state.searchState.images
            tableView.reloadData()
            store.dispatch(SearchAction.Rendered())
        case .failed:
            images?.removeAll()
            tableView.reloadData()
            
            let alert = UIAlertController(title: "Error", message: "Something didn't work quite right", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
                store.dispatch(SearchAction.NotStarted())
            }))
            alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { action in
                store.dispatch(SearchAction.Fetch())
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
        searchSpinner.isHidden = state.searchState.status != .inProgress
        if state.searchState.status == .inProgress {
            searchSpinner.startAnimating()
        } else {
            searchSpinner.stopAnimating()
        }
    }
}

