//
//  SearchTableViewCell.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/10/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import UIKit
import AVKit

class SearchTableViewCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    private var gradientAdded = false
    
    var model: ImageSearchResultModel? {
        didSet {
            label?.text = model?.title
            if let url = URL(string: model?.allImages.first?.link ?? "") {
                switch model?.firstType {
                case .videoMp4?:
                    DispatchQueue.global().async {
                        let asset = AVAsset(url: url)
                        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
                        assetImgGenerate.appliesPreferredTrackTransform = true
                        let time = CMTimeMake(value: 1, timescale: 2)
                        let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
                        if img != nil {
                            let frameImg  = UIImage(cgImage: img!)
                            DispatchQueue.main.async(execute: { [weak self] in
                                // assign your image to UIImageView
                                self?.thumbnailImageView?.image = frameImg
                            })
                        } else {
                            DispatchQueue.main.async(execute: { [weak self] in
                                self?.thumbnailImageView?.image = UIImage(placeholder: .default)
                            })
                        }
                    }
                default:
                    thumbnailImageView?.setImage(url: url) { [weak self] result in
                        switch result {
                        case .error:
                            self?.thumbnailImageView?.image = UIImage(placeholder: .default)
                        default: break
                        }
                        self?.setNeedsDisplay()
                    }
                }
            } else {
                thumbnailImageView?.image = UIImage(placeholder: .default)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        label.textColor = UIColor.white
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addGradient()
    }
    
    
    fileprivate func addGradient() {
        if gradientAdded { return }
        
        let w = frame.width
        let h = frame.height
        let h2 = h * 0.65
        
        let layer = CAGradientLayer()
        layer.frame = CGRect(x: CGFloat(0), y: CGFloat(h - h2), width: CGFloat(w), height: CGFloat(h2))
        let blackColor = UIColor(white: 0, alpha: 0.75)
        let clearColor = UIColor.clear
        
        layer.colors = [clearColor.cgColor, blackColor.cgColor]
        layer.startPoint = .zero
        layer.endPoint = CGPoint(x: 0.0, y: 0.5)
        gradientAdded = true
        
        thumbnailImageView!.layer.addSublayer(layer)
    }
}
