//
//  SearchActions.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/8/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import ReSwift

struct SearchAction {
    static var debounceMilliseconds = 250
    
    struct SetQuery: Action {
        let query: String?
    }
    
    struct Fetch: Action {
        var query: String?
    }
    
    struct HandleResponse: Action {
        let response: ImageSearchResponse
    }
    
    struct FetchNextPage: Action {}
    struct Rendered: Action {}
    struct FetchError: Action {}
    struct NotStarted: Action {}
}
