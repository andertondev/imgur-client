//
//  SearchMiddleware.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/5/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//
import ReSwift
import Alamofire


let searchMiddleware: Middleware<StateType> = { dispatch, getState in
    var state: SearchState? { return store.state.searchState }
    
    return { next in
        return { action in
            next(action)
            
            switch action {
            case let action as SearchAction.SetQuery:
                if let block = state?.debounceDelay {
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(SearchAction.debounceMilliseconds), execute: block)
                }
            case let action as SearchAction.Fetch:
                guard let query = state?.query, !query.isEmpty else { break }
                guard !(state?.lastPageFound ?? false) else {
                    break
                }
                
                let url = String(format: Api.searchUrl, state?.page ?? 0)
                let parameters: Parameters = ["q": query]
                
                DispatchQueue.global(qos: .userInitiated).async {
                    Alamofire.request(url,
                      parameters: parameters,
                      encoding: URLEncoding(destination: .queryString),
                      headers: Api.defaultHeaders).responseData { response in
                        
                        guard query == state?.query else {
                            // this handles the case where a long running query returns after a more recent, query
                            // we only want to show the results from the last user initiated query, not the last responding query
                            return
                        }
                        
                        let decoder = JSONDecoder()
                        
                        DispatchQueue.main.async {
                            if let data = response.data, let responseObject = try? decoder.decode(ImageSearchResponse.self, from: data) {
                                store.dispatch(SearchAction.HandleResponse(response: responseObject))
                            } else {
                                store.dispatch(SearchAction.FetchError())
                            }
                        }
                    }
                }
                
            case _ as SearchAction.FetchNextPage:
                store.dispatch(SearchAction.Fetch())
            default: break
            }
        }
    }
}
