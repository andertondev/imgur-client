//
//  SearchState.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/5/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import ReSwift

struct SearchState: StateType {
    var query: String?
    var page: Int = 0
    var debounceDelay: DispatchWorkItem?
    var status: SearchStatus = .notStarted
    var images: [ImageSearchResultModel]?
    var lastPageFound = false
    var selected: ImageSearchResultModel?
}

enum SearchStatus {
    case notStarted,
    debouncing,
    inProgress,
    completed(ImageSearchResponse),
    failed(ImageSearchError),
    rendered
}

extension SearchStatus: Equatable {
    static func ==(lhs: SearchStatus, rhs: SearchStatus) -> Bool {
        switch (lhs, rhs) {
        case (.notStarted, .notStarted):
            return true
        case (.debouncing, .debouncing):
            return true
        case (.inProgress, .inProgress):
            return true
        case (.completed, .completed):
            return true
        case (.failed, .failed):
            return true
        case (.rendered, .rendered):
            return true
        default:
            return false
        }
    }
}


