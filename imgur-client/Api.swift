//
//  Api.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/8/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import Foundation

struct Api {
    static var searchUrl = "https://api.imgur.com/3/gallery/search/time/%d"
    static var defaultHeaders = ["Authorization": "Client-ID 126701cd8332f32"]
}
