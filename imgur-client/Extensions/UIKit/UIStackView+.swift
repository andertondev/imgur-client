//
//  UIStackView+.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/9/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import UIKit

extension UIStackView {
    func removeAllArrangedViews() {
        _ = arrangedSubviews.map { view in
            removeArrangedSubview(view)
            view.removeFromSuperview()
        }
    }
}
