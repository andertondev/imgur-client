//
//  UIImage+.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/9/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import UIKit
extension UIImage {
    convenience init?(placeholder placeholderType: UIImagePlaceHolderType) {
        self.init(named: placeholderType.rawValue)
    }
    
    enum UIImagePlaceHolderType: String {
        case `default` = "imagePlaceholder",
        notFound = "imageNotFound"
        
    }
}
