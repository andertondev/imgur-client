//
//  String+.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/5/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import Foundation
extension String {
    var capitalizeFirst: String {
        return prefix(1).uppercased() + dropFirst()
    }
}
