//
//  ImageSearchResultModel.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/5/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import Foundation
import Alamofire

struct ImageSearchResultModel: Codable {
    let title: String?
    let type: ImageAssetType?
    let images: [ImageModel]?
    let link: String?
}

extension ImageSearchResultModel {
    var hasMultipleImages: Bool {
        return images?.count ?? 0 > 0
    }
    
    var allImages: [ImageModel] {
        if let images = images {
            return images
        } else {            
            return [ImageModel(title: title, type: type, link: link)]
        }
    }
    
    var firstType: ImageAssetType? {
        return allImages.first?.type
    }
}

struct ImageSearchResponse: Codable {
    let success: Bool
    let status: Int
    let data: [ImageSearchResultModel]?
}

enum ImageSearchError {
    case httpRequestFailed,
    invalidResponse,
    unknown
}
