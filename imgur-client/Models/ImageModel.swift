//
//  ImageModel.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/5/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import Foundation
struct ImageModel: Codable {
    let title: String?
    let type: ImageAssetType?
    let link: String?
}

enum ImageAssetType: String, Codable, Equatable {
    case imageGif = "image/gif",
    imageJpeg = "image/jpeg",
    imageJpg = "image/jpg",
    imagePng = "image/png",
    videoMp4 = "video/mp4",
    unsupported
}
