//
//  AppState.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/5/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import ReSwift

struct AppState: StateType {
    let actionState: ActionState
    let searchState: SearchState
    let detailState: DetailState
}
