//
//  AppMiddlewares.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/5/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import ReSwift

let appMiddlewares: [(@escaping DispatchFunction, @escaping () -> AppState?) -> (@escaping DispatchFunction) -> DispatchFunction] = [
    actionMiddleware,
    searchMiddleware
]
