//
//  AppReducer.swift
//  imgur-client
//
//  Created by Jared Anderton on 6/5/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import ReSwift

func appReducer(action: Action, state: AppState?) -> AppState {
    return AppState(
        actionState: actionReducer(action: action, state: state?.actionState),
        searchState: searchReducer(action: action, state: state?.searchState),
        detailState: detailReducer(action: action, state: state?.detailState)
    )
}
