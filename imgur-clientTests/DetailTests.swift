//
//  imgur_clientTests.swift
//  imgur-clientTests
//
//  Created by Jared Anderton on 6/5/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import XCTest
import ReSwift
@testable import imgur_client


class DetailTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        store = Store<AppState>(reducer: appReducer, state: nil)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        store = Store<AppState>(reducer: appReducer, state: nil)
    }

    func testInitialDetailIsNil() {
        XCTAssertNil(store.state.detailState.image, "Initial detail image was not nil")
    }
    
    func testInitialDetailSetSelectedAction() {
        let image = MockFactory().mockJpegs[0]
        
        
        
        store.dispatch(DetailAction.SetSelected(image: image))
        XCTAssertNotNil(store.state.detailState.image, "Detail state image was nil after dispatching set action")
        
        store.dispatch(DetailAction.SetSelected(image: nil))
        XCTAssertNil(store.state.detailState.image, "Detail state image was not nil after dispatching reset action")
    }
    
    func testJpegSingle() {
        let jpeg = MockFactory().mockJpegs[0]
        XCTAssertTrue(jpeg.type == ImageAssetType.imageJpeg, "image was not a jpeg")
        XCTAssertNil(jpeg.images, "found images when we already had a type")
        XCTAssertFalse(jpeg.hasMultipleImages, "multiple images found")
    }
    
    func testJpegMultiple() {
        let jpeg = MockFactory().mockJpegs[1]
        XCTAssertNil(jpeg.type, "found a type, when we have a list of images")
        XCTAssertTrue(jpeg.images![0].type == ImageAssetType.imageJpeg, "found non jpeg")
        XCTAssertTrue(jpeg.hasMultipleImages, "single images found")
    }
    
    func testJpgSingle() {
        let jpg = MockFactory().mockJpgs[0]
        XCTAssertTrue(jpg.type == ImageAssetType.imageJpg, "image was not a jpg")
        XCTAssertNil(jpg.images, "found images when we already had a type")
        XCTAssertFalse(jpg.hasMultipleImages, "multiple images found")
    }
    
    func testJpgMultiple() {
        let jpg = MockFactory().mockJpgs[1]
        XCTAssertNil(jpg.type, "found a type, when we have a list of images")
        XCTAssertTrue(jpg.images![0].type == ImageAssetType.imageJpg, "found non jpg")
        XCTAssertTrue(jpg.hasMultipleImages, "single images found")
    }
    
    func testGifSingle() {
        let gif = MockFactory().mockGifs[0]
        XCTAssertTrue(gif.type == ImageAssetType.imageGif, "image was not a gif")
        XCTAssertNil(gif.images, "found images when we already had a type")
        XCTAssertFalse(gif.hasMultipleImages, "multiple images found")
    }
    
    func testGifMultiple() {
        let gif = MockFactory().mockGifs[1]
        XCTAssertNil(gif.type, "found a type, when we have a list of images")
        XCTAssertTrue(gif.images![0].type == ImageAssetType.imageGif, "found non gif")
        XCTAssertTrue(gif.hasMultipleImages, "single images found")
    }
    
    
    func testPngSingle() {
        let png = MockFactory().mockPngs[0]
        XCTAssertTrue(png.type == ImageAssetType.imagePng, "image was not a png")
        XCTAssertNil(png.images, "found images when we already had a type")
        XCTAssertFalse(png.hasMultipleImages, "multiple images found")
    }
    
    func testPngMultiple() {
        let png = MockFactory().mockPngs[1]
        XCTAssertNil(png.type, "found a type, when we have a list of images")
        XCTAssertTrue(png.images![0].type == ImageAssetType.imagePng, "found non png")
        XCTAssertTrue(png.hasMultipleImages, "single images found")
    }
    
    func testMp4Single() {
        let mp4 = MockFactory().mockMp4s[0]
        XCTAssertTrue(mp4.type == ImageAssetType.videoMp4, "video was not a mp4")
        XCTAssertNil(mp4.images, "found videos when we already had a type")
        XCTAssertFalse(mp4.hasMultipleImages, "multiple images found")
    }
    
    func testMp4Multiple() {
        let mp4 = MockFactory().mockMp4s[1]
        XCTAssertNil(mp4.type, "found a type, when we have a list of videos")
        XCTAssertTrue(mp4.images![0].type == ImageAssetType.videoMp4, "found non mp4")
        XCTAssertTrue(mp4.hasMultipleImages, "single images found")
        
    }

}


class MockFactory {
    func getDataForUrl(_ filename: String, _ fileExtension: String) -> Data {
        let bundle = Bundle(for: type(of: self))
        let url = bundle.url(forResource: filename, withExtension: fileExtension)!
        return try! Data(contentsOf: url)
    }
    
    var mockJpegs: [ImageSearchResultModel] {
        return try! JSONDecoder().decode(
            ImageSearchResponse.self,
            from: getDataForUrl("mock.jpeg", "json")).data!
    }
    
    var mockJpgs: [ImageSearchResultModel] {
        return try! JSONDecoder().decode(
            ImageSearchResponse.self,
            from: getDataForUrl("mock.jpg", "json")).data!
    }
    
    var mockGifs: [ImageSearchResultModel] {
        return try! JSONDecoder().decode(
            ImageSearchResponse.self,
            from: getDataForUrl("mock.gif", "json")).data!
    }
    
    var mockPngs: [ImageSearchResultModel] {
        return try! JSONDecoder().decode(
            ImageSearchResponse.self,
            from: getDataForUrl("mock.png", "json")).data!
    }
    
    var mockMp4s: [ImageSearchResultModel] {
        return try! JSONDecoder().decode(
            ImageSearchResponse.self,
            from: getDataForUrl("mock.mp4", "json")).data!
    }
    
    var mockLastPage: [ImageSearchResultModel] {
        return try! JSONDecoder().decode(
            ImageSearchResponse.self,
            from: getDataForUrl("mock.lastPage", "json")).data!
    }
}
