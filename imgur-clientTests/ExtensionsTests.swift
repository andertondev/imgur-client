//
//  ImageTests.swift
//  imgur-clientTests
//
//  Created by Jared Anderton on 6/9/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import XCTest
import ReSwift
@testable import imgur_client


class ExtensionsTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        store = Store<AppState>(reducer: appReducer, state: nil)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        store = Store<AppState>(reducer: appReducer, state: nil)
    }
    
    func testStringCapitalizeFirst() {
        XCTAssertEqual("upperCamelCase".capitalizeFirst, "UpperCamelCase")
        XCTAssertEqual("upper".capitalizeFirst, "Upper")
    }
    
    func testUIStackViewRemoveAllArrangedViews() {
        let stackView = UIStackView()
        XCTAssertEqual(stackView.arrangedSubviews.count, 0)
        let view = UIView()
        stackView.addArrangedSubview(view)
        XCTAssertEqual(stackView.arrangedSubviews.count, 1)
        
        stackView.removeAllArrangedViews()
        XCTAssertEqual(stackView.arrangedSubviews.count, 0)
    }
    
    func testUIImagePlaceHolders() {
        XCTAssertNotNil(UIImage(placeholder: .default))
    }
}
