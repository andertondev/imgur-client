//
//  imgur_clientTests.swift
//  imgur-clientTests
//
//  Created by Jared Anderton on 6/5/19.
//  Copyright © 2019 Anderton Development. All rights reserved.
//

import XCTest
import ReSwift
@testable import imgur_client


class SearchTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        store = Store<AppState>(reducer: appReducer, state: nil)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        store = Store<AppState>(reducer: appReducer, state: nil)
    }

    func testInitialQueryIsNil() {
        XCTAssertNil(store.state.searchState.query, "Initial detail image was not nil")
        XCTAssertEqual(store.state.searchState.status, SearchStatus.notStarted)
    }
    
    func testQueryAction() {
        store.dispatch(SearchAction.SetQuery(query: "a"))
        XCTAssertEqual(store.state.searchState.query, "a", "Search action did not update query in state")
        XCTAssertEqual(store.state.searchState.status, SearchStatus.debouncing)
        XCTAssertEqual(store.state.searchState.images?.count ?? 0, 0, "images found after changing query")
        XCTAssertFalse(store.state.searchState.lastPageFound, "last page indicated after changing query")
    }
    
    func testEmptyQueryAction() {
        store.dispatch(SearchAction.SetQuery(query: ""))
        XCTAssertEqual(store.state.searchState.status, SearchStatus.notStarted)
    }
    
    func testFetchAction() {
        store.dispatch(SearchAction.Fetch(query: "a"))
        XCTAssertEqual(store.state.searchState.status, SearchStatus.inProgress)
    }
    
    func testNotStartedAction() {
        store.dispatch(SearchAction.NotStarted())
        XCTAssertEqual(store.state.searchState.status, SearchStatus.notStarted)
    }
    
    func testRenderedAction() {
        store.dispatch(SearchAction.Rendered())
        XCTAssertEqual(store.state.searchState.status, SearchStatus.rendered)
    }
    
    func testFetchNextPageAction() {
        XCTAssertEqual(store.state.searchState.page, 0)
        store.dispatch(SearchAction.FetchNextPage())
        XCTAssertEqual(store.state.searchState.page, 1)
    }
    
    func testValidResponse() {
        let response = try! JSONDecoder().decode(ImageSearchResponse.self, from: MockFactory().getDataForUrl("mock.jpeg", "json"))
        store.dispatch(SearchAction.HandleResponse(response: response))
        XCTAssertEqual(store.state.searchState.status, SearchStatus.completed(response))
    }
    
    func testValidResponseAddingImages() {
        XCTAssertNil(store.state.searchState.images)
        let response = try! JSONDecoder().decode(ImageSearchResponse.self, from: MockFactory().getDataForUrl("mock.jpeg", "json"))
        
        store.dispatch(SearchAction.HandleResponse(response: response))
        XCTAssertEqual(store.state.searchState.images!.count, 2)
        
        store.dispatch(SearchAction.HandleResponse(response: response))
        XCTAssertEqual(store.state.searchState.images!.count, 4)
        
        store.dispatch(SearchAction.HandleResponse(response: response))
        XCTAssertEqual(store.state.searchState.images!.count, 6)
    }
    
    func testValidResponseAddingImagesAndLastPage() {
        XCTAssertNil(store.state.searchState.images)
        let response = try! JSONDecoder().decode(ImageSearchResponse.self, from: MockFactory().getDataForUrl("mock.jpeg", "json"))
        let responseLastPage = try! JSONDecoder().decode(ImageSearchResponse.self, from: MockFactory().getDataForUrl("mock.lastPage", "json"))
        
        store.dispatch(SearchAction.HandleResponse(response: response))
        XCTAssertEqual(store.state.searchState.images!.count, 2)
        XCTAssertFalse(store.state.searchState.lastPageFound)
        
        store.dispatch(SearchAction.HandleResponse(response: responseLastPage))
        XCTAssertEqual(store.state.searchState.images!.count, 2)
        XCTAssertTrue(store.state.searchState.lastPageFound)
        
        
    }
    
    func testInvalidResponseAddingImages() {
        XCTAssertNil(store.state.searchState.images)
        let response = try? JSONDecoder().decode(ImageSearchResponse.self, from: MockFactory().getDataForUrl("mock.bmp", "json"))
        XCTAssertNil(response)
    }
    
    func testFetchError() {
        XCTAssertEqual(store.state.searchState.status, SearchStatus.notStarted)
        let error = ImageSearchError.unknown
        store.dispatch(SearchAction.FetchError())
        XCTAssertEqual(store.state.searchState.status, SearchStatus.failed(error))
    }
    
    func testSearchStatusEquatable() {
        XCTAssertNotEqual(SearchStatus.notStarted, SearchStatus.debouncing)
    }
    
    
//    func testDebounce() {
//        XCTAssertEqual(store.state.searchState.status, SearchStatus.notStarted)
//        store.dispatch(SearchAction.SetQuery(query: "a"))
//        XCTAssertEqual(store.state.searchState.status, SearchStatus.debouncing)
//
//        let ms = 1000
//        usleep(useconds_t(SearchAction.debounceMilliseconds * ms)+10000)
//        XCTAssertEqual(store.state.searchState.status, SearchStatus.inProgress)
//
//
//    }
}



