echo "Test Coverage: "$(find . -name 'action.xccovreport' -exec xcrun xccov view $1 --only-targets -- {} \; | grep .app | grep -o -E '[0-9]{2,3}[.][0-9]{2}')"%"
